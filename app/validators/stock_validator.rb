class StockValidator < ActiveModel::Validator
  def validate(record)
    record.allocations.each do |allocation|
      product = allocation.product

      if product.nil?
        record.errors.add(:base, :invalid_product)
      else
        if allocation.quantity > product.quantity
          record.errors[:base] << I18n.t('activerecord.errors.out_of_stock', product: product.name)
        end
      end
    end
  end
end
