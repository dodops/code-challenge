class AddQuantityToAllocations < ActiveRecord::Migration[5.2]
  def change
    add_column :allocations, :quantity, :integer, default: 0
  end
end
