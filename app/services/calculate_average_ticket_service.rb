class CalculateAverageTicketService < ApplicationService
  attr_accessor :start_at, :end_at

  def initialize(params)
    @start_at = params[:start_at]
    @end_at   = params[:end_at] 
  end

  def call
    { 
      start_at: start_at,
      end_at: end_at,
      average_ticket: ActionController::Base.helpers.number_to_currency(average_ticket)
    }
  end

  private

  def orders
    @orders = Order.all

    by_starting_at
    by_ending_at

    @orders
  end

  def average_ticket
    total = orders.map(&:total).sum
    return total if total <= 0
    
    (total / orders.count).round(2)
  end

  def by_starting_at
    return if start_at.blank?

    @orders = @orders.where(
      'DATE(created_at) >= ?', dateparse(start_at).beginning_of_day
    )
  end

  def by_ending_at
    return if end_at.blank?

    @orders = @orders.where('DATE(created_at) <= ?', dateparse(end_at).end_of_day)
  end

  def dateparse(date)
    Date.parse(date)
  end
end
