require 'rails_helper'

RSpec.describe Allocation, type: :model do
  describe 'relationships' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:order) }
  end

  describe "#decrement_product_quantity!" do
    it 'should decrement the product quantity associated' do
      order       = create :order
      product     = create :product, quantity: 20
      allocation  = build :allocation, order: order, product: product, quantity: 1

      expect{
        allocation.save
      }.to change(product, :quantity).by(-1)
    end
  end

  describe "#increment_product_quantity!" do
    it 'should increment the product quantity associated' do
      order       = create :order
      product     = create :product, quantity: 20
      allocation  = create :allocation, order: order, product: product, quantity: 1

      expect{
        allocation.destroy
      }.to change(product, :quantity).by(1)
    end
  end
end
