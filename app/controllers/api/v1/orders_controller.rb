class Api::V1::OrdersController < ApplicationController
  before_action :find_order, only: [:show, :destroy, :update]

  def index
    render json: Order.all, status: :ok
  end

  def show
    render json: @order, status: :ok
  end

  def create
    form = Order::CreateForm.new(order_params)

    if form.save
      render json: form.order, status: :created, location: [:api, form.order]
    else
      render json: { errors: form.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    if @order.update(order_update_params)
      render json: @order, status: :ok, location: [:api, @order]
    else
      render json: { errors: @order.errors.full_messages }, status: :unprocessable_entity, location: [:api, @order]
    end
  end

  def destroy
    @order.destroy

    head 204
  end

  private

  def order_params
    params.require(:order).permit(:code, :shipment_price,
                                  :total, :status, :customer,
                                  products: %i[code quantity price id])
  end

  def order_update_params
    params.require(:order).permit(:code, :shipment_price,
                                  :total, :status, :customer)
  end

  def find_order
    @order = Order.find(params[:id])
  end
end
