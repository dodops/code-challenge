class ChangeStatusColumn < ActiveRecord::Migration[5.2]
	def up
    remove_column :orders, :aasm_state
    add_column :orders, :status, :integer, default: 0
  end

  def down
    remove_column :orders, :status
    add_column :orders, :aasm_state, :string
  end
end
