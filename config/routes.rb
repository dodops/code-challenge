Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    scope module: :v1 do
      resources :products

      resources :orders

      resource :average_ticket, only: :show
    end
  end
end
