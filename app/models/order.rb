class Order < ApplicationRecord
  before_save :set_total!

  enum status: [:new_order, :approved, :delivered, :canceled]

  has_many :allocations, dependent: :destroy
  has_many :products, through: :allocations

  validates :customer, presence: true

  validates_with StockValidator

  def build_allocation(ids_and_quantities)
    ids_and_quantities.each do |id_and_quantity|
      id, quantity = id_and_quantity

      self.allocations.build(product_id: id, quantity: quantity)
    end
  end

  def set_total!
    value = 0

    allocations.each do |allocation|
      value += (allocation.product.price * allocation.quantity)
    end

    self.total = value + shipment_price
  end
end
