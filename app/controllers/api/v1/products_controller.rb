class Api::V1::ProductsController < ApplicationController
  before_action :find_product, only: [:show, :update, :destroy]

  def show
    render json: @product, status: :ok
  end

  def index
    render json: Product.all, status: :ok
  end

  def create
    product = Product.new(product_params)

    if product.save
      render json: product, status: :created, location: [:api, product]
    else
      render json: { errors: product.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @product.update(product_params)
      render json: @product, status: :ok, location: [:api, @product]
    else
      render json: { errors: @product.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @product.destroy

    head 204
  end

  private

  def product_params
    params.require(:product).permit(:name, :code, :price, 
                                    :description, :quantity, data: {})
  end

  def find_product
    @product = Product.find(params[:id])
  end
end
