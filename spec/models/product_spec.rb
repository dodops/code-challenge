require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:price) }
  end

  describe 'relationships' do
    it { is_expected.to have_many(:allocations) }
    it { is_expected.to have_many(:orders).through(:allocations) }
  end
end
