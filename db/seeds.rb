# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "======== Criando Produtos ============"
20.times do |i|
  name        = Faker::Commerce.product_name
  description = Faker::Lorem.sentence
  quantity    = Faker::Number.between(10, 50)
  price       = Faker::Commerce.price
  code        = Faker::Code.ean
  ppp         = Product.create(code: code, name: name, description: description, quantity: quantity, price: price)
end
puts "======== Produtos criados ============"

puts "======== Criando Pedidos ============"
Product.all.each do |product|
  order = Order.new(code: Faker::Code.ean, customer: Faker::Name.name, shipment_price: '10.0')
  alloc = order.allocations.build(product_id: product.id, quantity: 1)
  order.save
  alloc.save
end
puts "Ok! :)"
