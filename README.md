# Zé Store

* Ruby on Rails E-commerce minimal example
* Rails 5 API only
* PostgreSQL
* Test coverage de 100% (ao rodar os testes é gerado do coverage/index.html)

## Configuração inicial

O projeto depende das sequintes libs:

* Ruby 2.6.3, ou superior.
* PostgreSQL deve ser instalado e rodando na porta default

Clonando e configurando o banco:

* Clonar o repo: `git clone https://dodops@bitbucket.org/dodops/code-challenge.git`
* Instalar as gems `cd code-challenge && bundle`
* Criar e migrar o banco de dados `bundle exec rails db:create db:migrate`

## Geração de dados de exemplo

* `rails db:seed`

## Rodando o projeto

1. Execute `rspec spec` para rodar os testes.
2. Execute `rails s` para iniciar a aplicação.


### Documentação da API

#### URL

[http://localhost:3000/](http://localhost:3000.)

#### Endpoints

##### Produtos

INDEX

```code
GET: http://localhost:3000/products
"http://localhost:3000/products"
```

Resposta:

```JSON
{
    "products": [
        {
            "id": 1,
            "code": "9138555461722",
            "description": "Repellendus saepe non consequuntur.",
            "price": "73.72",
            "data": null,
            "name": "Intelligent Wooden Lamp",
            "quantity": 49
        },
        {
            "id": 2,
            "code": "7228088660388",
            "description": "Aspernatur aut repellat velit.",
            "price": "23.69",
            "data": null,
            "name": "Lightweight Concrete Gloves",
            "quantity": 14
        }
    ]
}
```
SHOW

```code
GET: http://localhost:3000/products/:id
```

Resposta:

```JSON
{
    "product": {
        "id": 1,
        "code": "9138555461722",
        "description": "Repellendus saepe non consequuntur.",
        "price": "73.72",
        "data": null,
        "name": "Intelligent Wooden Lamp",
        "quantity": 49
    }
}
```

CREATE

```code
POST: http://localhost:3000/products
Params: Body, JSON(application/json)
name, quantity, price and code are required
```

```json
{
	"name": "Tela",
	"code": "309434232",
	"description": "uma tela",
	"price": "25.00",
	"quantity": 2,
    "data": {
      "tamanho": "grande"
  }
}
```

Resposta:

```JSON
{
    "product": {
        "id": 21,
        "code": "309434232",
        "description": "uma tela",
        "price": "25.0",
        "data": {
            "tamanho": "grande"
        },
        "name": "Tela",
        "quantity": 2
    }
}
```

UPDATE

```code
PUT: http://localhost:3000/products/1
Params: Body, JSON(application/json)
```

```json
{
	"product": {
		"quantity": 70
	}
}
```

Resposta:

```JSON
{
    "product": {
        "id": 21,
        "code": "309434232",
        "description": "uma caneca muito engraçada",
        "price": "25.0",
        "data": {
            "tamanho": "média"
        },
        "name": "Tela",
        "quantity": 70
    }
}
```

DESTROY

```code
DELETE: http://localhost:3000/products/2
```

Resposta:

```code
204 No Content
```

##### Pedidos

###### Atributos:
```code
code: string
customer: String
status: String: [new_order, canceled, approved, delivered]
shipment_price: Float
products: array de produtos
```

INDEX

```code
GET: http://localhost:3000/orders
```

Resposta:

```JSON

"orders": [
    {
        "id": 1,
        "code": "2952179392227",
        "customer": "Joana da Gama",
        "status": "new_order",
        "shipment_price": "10.0",
        "total": "83.72",
        "created_at": "2019-06-19T15:00:14.419-03:00",
        "products": [
            {
                "product_id": 1,
                "quantity": 1
            }
        ]
      },
      {
        "id": 2,
        "code": "9860199967680",
        "customer": "Nataniel da Paz",
        "status": "new_order",
        "shipment_price": "10.0",
        "total": "33.69",
        "created_at": "2019-06-19T15:00:14.439-03:00",
        "products": [
            {
                "product_id": 2,
                "quantity": 1
            }
        ]
    }
]
```
CREATE

```code
POST: http://DOMAIN/customers
"http://localhost:3000/customers"
Params: Body, JSON(application/json)
code, customer, shipment_price and products are required
```

```json
{
	"order": {
		"code": "oo3i434io",
		"customer": "Sandro Silva",
		"shipment_price": "10.0",
		"products": [
			{
				"id": 1,
				"quantity":1
			}]
	}
}

```

Resposta:

```json
{
    "order": {
        "id": 24,
        "code": "4923849234",
        "customer": "Sandro Silva",
        "status": "new_order",
        "shipment_price": "10.0",
        "total": "83.72",
        "created_at": "2019-06-19T16:58:07.301-03:00",
        "products": [
            {
                "product_id": 1,
                "quantity": 1
            }
        ]
    }
}
```

DESTROY

```code
DELETE: http://localhost:3000/orders/:id
```

Resposta:

```code
204 No Content
```

##### To Reports

AVERAGE_TICKET

```code
GET:
http://localhost:3000/api/average_ticket?start_at=12/05/2019&end_at=12/09/2019
Params: Body, JSON(application/json)
start_at or end_at are required, both are accepted to create a range
```

Resposta:

```json
{
    "start_at": "12/05/2019",
    "end_at": "12/09/2019",
    "average_ticket": "R$ 63,99"
}
```
