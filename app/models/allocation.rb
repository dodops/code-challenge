class Allocation < ApplicationRecord
  after_create :decrement_product_quantity!
  before_destroy :increment_product_quantity!

  belongs_to :order
  belongs_to :product

  private

  def decrement_product_quantity!
    self.product.decrement!(:quantity, quantity)
  end

  def increment_product_quantity!
    self.product.increment!(:quantity, quantity)
  end
end
