require 'rails_helper'

RSpec.describe StockValidator do
  let(:order) { build :order }

  context 'when product is not found' do
    before do
      order.build_allocation([[100201, 1]])
      order.valid?
    end

    it 'makes the object invalid' do
      expect(order).to be_invalid
    end

    it 'add errors' do
      expect(order.errors.full_messages).to include "Produto informado não cadastrado"
    end
  end

  context 'when quantity in stock is not enough' do
    let(:product) { create :product, name: 'Sofá', quantity: 3 }

    before do
      order.build_allocation([[product.id, 4]])
      order.valid?
    end

    it 'makes the object invalid' do
      expect(order).to be_invalid
    end

    it 'add errors' do
      expect(order.errors.full_messages).to include "A quantidade do produto: Sofá é insuficiente no estoque"
    end
  end

  context 'when product is found and has enough stock' do
    let(:product) { create :product, name: 'Mesa', quantity: 3 }

    before do
      order.build_allocation([[product.id, 1]])
      order.valid?
    end

    it 'makes the object valid' do
      expect(order).to be_valid
    end
  end
end
