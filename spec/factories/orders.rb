FactoryBot.define do
  factory :order do
    code { Faker::Code.ean }
    customer { Faker::Name.name }
    shipment_price { "10.0" }

    factory :order_with_products do
      after(:build) do |order, evaluator|
        product = create :product, quantity: 10
        create(:allocation, product: product, order: order, quantity: 1)
      end
    end
  end
end
