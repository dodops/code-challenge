class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string  :code
      t.string  :name
      t.text    :description
      t.decimal :price, precision: 8, scale: 2
      t.jsonb   :data
      t.string  :aasm_state

      t.timestamps
    end
  end
end
