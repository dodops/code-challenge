class Product < ApplicationRecord
  has_many :allocations
  has_many :orders, through: :allocations 

  validates :name, :price, :code, :quantity, presence: true
  validates_uniqueness_of :code
end
