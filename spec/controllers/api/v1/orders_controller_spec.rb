require 'rails_helper'

RSpec.describe Api::V1::OrdersController, type: :controller do
  describe "POST #create" do
    let(:product_1) { create :product, quantity: 2 }
    let(:product_2) { create :product, quantity: 3 }

    context 'when user informs right params' do
      let(:order_params) do
        { 
          "shipment_price": 19.0,
          "customer": 'Jamile',
          "products": [
            { "id": product_1.id, "quantity": 1 },
            { "id": product_2.id, "quantity": 1 }
          ] 
        }
      end

      before { post :create, params: { order: order_params } }

      it "returns the order record" do
        order_response = json_response[:order]

        expect(order_response[:id]).to be_present
      end

      it { is_expected.to respond_with 201 }
    end


    context 'when user informs wrong data' do
      let(:order_params) do
        {
          "shipment_price": 5.30,
          "products": [
            {
              "id": product_1.id,
              "quantity": 10001
            }
          ]
        }
      end

      it 'returns error http status' do
        post :create, params: { order: order_params }

        expect(response).to have_http_status 422
      end
    end
  end

  describe "PUT #update" do
    let(:order) { create :order_with_products }

    context 'when update is successful' do
      let(:valid_params) { { "customer": 'Paulo' } }

      before { put :update, params: { id: order.id, order: valid_params } }

      it 'updates the resource object' do
        expect(json_response[:order][:customer]).to eq 'Paulo'
      end

      it 'returns ok status code' do
        expect(response).to have_http_status 200
      end
    end

    context 'when update is not successful' do
      let(:invalid_params) { { "customer": ' ' } }

      before { put :update, params: { id: order.id, order: invalid_params } }

      it 'returns unprocessable_entity status code' do
        expect(response).to have_http_status 422
      end
    end
  end

  describe "GET #index" do
    let!(:orders) { create_list :order, 2 }

    before { get :index }

    it "returns order records" do
      orders_response = json_response[:orders]

      expect(orders_response.size).to eq 2
    end

    it { is_expected.to respond_with 200 }
  end

  describe "GET #show" do
    context 'when order is found' do
      let!(:order) { create :order }
      before do
        get :show, params: { id: order.id }
      end

      it "returns the  order record matching the id" do
        order_response = json_response[:order]
        expect(order_response[:id]).to eql order.id
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when order is not found' do
      it 'returns not found code' do
        get :show, params: { id: 343434 }

        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:order) { create :order }

    it 'returns 204 status code' do
      delete :destroy, params: { id: order.id }

      expect(response.status).to eq 204
    end
  end
end
