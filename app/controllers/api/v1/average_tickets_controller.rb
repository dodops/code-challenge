class Api::V1::AverageTicketsController < ApplicationController
  def show
    result = CalculateAverageTicketService.call(ticket_params)

    render json: result, status: :ok
  end

  private

  def ticket_params
    params.permit(:start_at, :end_at)
  end
end
