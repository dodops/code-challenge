class Order::CreateForm < ApplicationForm
  attr_accessor :code, :shipment_price, :customer,
                :products, :status, :total, :order

  validates :products, :customer, :shipment_price, presence: true

  def product_ids_and_quantity
    products.collect{ |product| [product['id'], product['quantity']] }
  end

  def save
    return false unless valid?

    @order ||= Order.new(customer: customer, code: code, shipment_price: shipment_price)
    @order.status = status if status
    @order.build_allocation(product_ids_and_quantity)

    @order.save
  end

  def self.model_name
    ActiveModel::Name.new(Order)
  end
end
