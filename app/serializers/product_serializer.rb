class ProductSerializer < ActiveModel::Serializer
  attributes :id, :code, :description, :price, :data, :name, :quantity
end
