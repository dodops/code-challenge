class AllocationSerializer < ActiveModel::Serializer
  attributes :product_id, :quantity
end
