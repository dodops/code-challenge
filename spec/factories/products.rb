FactoryBot.define do
  factory :product do
    code { Faker::Code.ean }
    name { Faker::Commerce.product_name }
    description { Faker::Lorem.sentence }
    price { Faker::Commerce.price }
    data { "" }
  end
end
