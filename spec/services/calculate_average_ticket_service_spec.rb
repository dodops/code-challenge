require 'rails_helper'

RSpec.describe CalculateAverageTicketService, type: :service do
  subject { described_class.call(params) }

  describe "#average_ticket" do
    let(:params) { { start_at: '01/01/2018', end_at: '01/01/2019' } }

    before do
      collection = spy(:collection)
      allow(Order).to receive(:all).and_return(collection)
      allow(collection).to receive(:count).and_return(3)
      allow(collection).to receive(:map).and_return([102.0, 902.30, 340.20])
    end

    context 'when orders is found' do
      it 'returns the average' do
        expect(subject[:average_ticket]).to eq "R$ 448,17"
      end
    end
  end

  describe 'filters by date' do
    let(:orders)  { double(:orders, map: []) }
    let(:date1) { Date.parse('01/01/2019') }
    let(:date2) { Date.parse('01/01/2018') }

    before { allow(Order).to receive(:all).and_return(orders) }

    context 'when start_at and end_at are nulls' do
      let(:params) { {} }

      it 'returns logs without executes query' do
        expect(orders).not_to receive(:where)

        subject
      end
    end

    context 'when both keys are presents' do
      let(:params) { { start_at: '01/01/2018', end_at: '01/01/2019' } }
      let(:orders)  { double(:orders, map: [], where: []) }

      it 'returns orders after executes both methods #by_starting_at and #by_ending_at' do
        expect(orders).to receive(:where).once.with(
          "DATE(created_at) >= ?", date2.beginning_of_day
        ).and_return(orders)

        expect(orders).to receive(:where).once.with(
          "DATE(created_at) <= ?", date1.end_of_day
        )

        subject
      end
    end
  end
end
