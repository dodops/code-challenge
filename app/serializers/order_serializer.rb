class OrderSerializer < ActiveModel::Serializer
  attributes :id, :code, :customer, :status, :shipment_price, :total, :created_at

  has_many :allocations, key: :products
end
