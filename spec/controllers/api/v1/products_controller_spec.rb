require 'rails_helper'

RSpec.describe Api::V1::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create :product }

    before { get :show, params: { id: product.id } }

    it "render json for a single product" do
      product_response = json_response[:product]

      expect(product_response[:name]).to eql product.name
    end

    it { is_expected.to respond_with 200 }
  end

  describe "GET #index" do
    let!(:products) { create_list :product, 4 }

    before { get :index }

    it "returns all records from the database" do
      products_response = json_response[:products]

      expect(products_response.size).to eq 4
    end

    it { is_expected.to respond_with 200 }
  end

  describe "POST #create" do
    context "when is successfully created" do
      let(:product_attributes) { attributes_for :product }

      before do
        post :create, params: { product: product_attributes }
      end

      it "renders the json representation for the product record just created" do
        product_response = json_response[:product]

        expect(product_response[:name]).to eql product_attributes[:name]
      end

      it { is_expected.to respond_with 201 }
    end

    context "when is not created" do
      let(:invalid_attributes) { { name: "Smartphone", price: "" } }

      before do
        post :create, params: { product: invalid_attributes }
      end

      it "renders an errors json" do
        product_response = json_response

        expect(product_response).to have_key(:errors)
      end

      it { is_expected.to respond_with 422 }
    end
  end

  describe "PATCH #update" do
    let!(:product) { create :product }

    context "when product is successfully updated" do
      before do
        patch :update, params: { id: product.id,
                                 product: { price: "100.15" } }
      end

      it "renders the json for the updated product" do
        product_response = json_response[:product]

        expect(product_response[:price]).to eql "100.15"
      end

      it { is_expected.to respond_with 200 }
    end

    context "when is not updated" do
      before do
        patch :update, params: { id: product.id,
                         product: { price: "" } }
      end

      it "renders an errors json" do
        product_response = json_response

        expect(product_response).to have_key(:errors)
      end

      it { is_expected.to respond_with 422 }
    end
  end

  describe "DELETE #destroy" do
    let!(:product) { create :product }

    before do
      delete :destroy, params: { id: product.id }
    end

    it { is_expected.to respond_with 204 }
  end
end
