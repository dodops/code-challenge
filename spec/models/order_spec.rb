require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'relationships' do
    it { is_expected.to have_many(:allocations) }
    it { is_expected.to have_many(:products).through(:allocations) }
  end

  describe '#set_total!' do
    it "returns the total amount of the order" do
      product_1 = create :product, price: 100
      product_2 = create :product, price: 85
      alloc1 = build :allocation, product: product_1, quantity: 1
      alloc2 = build :allocation, product: product_2, quantity: 3

      order = build :order, shipment_price: 10.50
      order.allocations << alloc1
      order.allocations << alloc2

      expect{
        order.set_total!
      }.to change{order.total.to_f}.from(0).to(365.50)
    end
  end
end
